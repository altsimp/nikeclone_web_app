import Link from "next/link";
import React from "react";

function Footer() {
  return (
    <footer className="w-full p-12 bg-black text-white ">
      <ul className="flex-1">
        <li className="">
          <Link href={"/"} className="text-white">
            <span>{`CARTES CADEAUX`}</span>
          </Link>
        </li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </footer>
  );
}

export default Footer;
